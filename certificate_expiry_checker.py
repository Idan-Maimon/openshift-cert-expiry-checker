import os
import requests
import json
import base64
from subprocess import Popen, PIPE
from prettytable import PrettyTable
from datetime import datetime, timedelta

def get_cluster_configs():
    with open("config.json", "r") as config_file:
        config_data = json.load(config_file)
        return config_data.get("clusters", [])

# Create a PrettyTable
table = PrettyTable(["Cluster", "Namespace", "Name", "Expiration Date"])

# Expiration threshold from the environment variable (default to 3 months if not set)
expiration_threshold_days = int(os.getenv("CERTIFICATE_EXPIRATION_THRESHOLD_DAYS", 3 * 30))
expiration_threshold = timedelta(days=expiration_threshold_days)

# List to store certificates about to expire
certificates_about_to_expire = []

# Process each cluster in the configuration
for cluster_config in get_cluster_configs():
    cluster_name = cluster_config.get("name", "")
    TOKEN = cluster_config.get("token", "")
    CLUSTER_DOMAIN_NAME = cluster_config.get("cluster_domain_name", "")

    url = f"https://api.{CLUSTER_DOMAIN_NAME}:6443/api/v1/secrets"
    headers = {"Authorization": f"Bearer {TOKEN}"}

    # Make the HTTP request
    response = requests.get(url, headers=headers, verify=False)
    data = response.json()

    # Process the JSON response for each cluster
    for item in data.get('items', []):
        if item.get('type') == 'kubernetes.io/tls':
            namespace = item.get('metadata', {}).get('namespace', '')
            name = item.get('metadata', {}).get('name', '')
            cert_data = item.get('data', {}).get('tls.crt', '')

            # Decode base64 and get the end date
            cert_bytes = base64.b64decode(cert_data)
            p = Popen(["openssl", "x509", "-noout", "-enddate"], stdin=PIPE, stdout=PIPE, stderr=PIPE)
            end_date, _ = p.communicate(cert_bytes)
            end_date = end_date.decode('utf-8').strip()

            # Parse the end date
            end_date = datetime.strptime(end_date[9:], '%b %d %H:%M:%S %Y %Z')

            # Check if the certificate is about to expire
            days_until_expire = (end_date - datetime.now()).days
            if days_until_expire <= expiration_threshold.days:
                certificates_about_to_expire.append((cluster_name, namespace, name, end_date))

            # Add a row to the table
            table.add_row([cluster_name, namespace, name, end_date.strftime('%b %d %H:%M:%S %Y %Z')])

# Set the alignment
table.align = "l"

# Print the table
print(table)

# Summary section for certificates about to expire
if certificates_about_to_expire:
    print("\nCertificates About to Expire:")
    for cert_info in certificates_about_to_expire:
        cluster_name, namespace, name, end_date = cert_info
        print(f"{cluster_name} | {namespace} | {name} | Expires on {end_date.strftime('%b %d %H:%M:%S %Y %Z')}")
else:
    print("\nNo Certificates are About to Expire.")

