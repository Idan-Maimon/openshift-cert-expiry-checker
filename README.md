# openshift-cert-expiry-checker


This script checks the expiration dates of TLS certificates in Openshift clusters and provides a summary of certificates about to expire.

## How it Works

The script makes use of the Kubernetes API to retrieve TLS certificates and their expiration dates. It then prints a table with detailed information about each certificate, including the cluster name, namespace, certificate name, and expiration date. Additionally, a summary section identifies certificates that are about to expire based on a specified threshold.

## Installation

- Apply the `certificate-checker-cronjob.yaml` and `rbac.yaml`
- Copy the service account token and insert it into the "token" field in the config.json file.
  * oc sa get-token secrets-checker-bot -n openshift    // on OCP version < 4.11.x
  * oc sa new-token secrets-checker-bot -n openshift   // on OCP version > 4.10.x
- create a secret named `certificate-checker-secret` out of the config.json file
```bash
oc create secret generic certificate-checker-secret --from-file=config.json
```
- Try the job
```bash
oc create job --from=cronjob/certificate-checker-cronjob test-job
```

## Configuration

### `config.json`

The configuration file, `config.json`, contains information about the clusters to check. Each cluster is defined with a name, token, and cluster domain name.

Example `config.json`:

```json
{
  "clusters": [
    {
      "name": "Cluster1",
      "token": "service-account_token_here",
      "cluster_domain_name": "cluster1.example.com"
    },
    {
      "name": "Cluster2",
      "token": "service-account_token_here",
      "cluster_domain_name": "cluster2.example.com"
    }
  ]
}
```

### Environment Variable 

CERTIFICATE_EXPIRATION_THRESHOLD_DAYS: (Optional) Set this environment variable to specify the expiration threshold in days. The default threshold is 90 days.

## Example output 

```
+----------+--------------------------------------------------+-----------------------------------------------------------------+-----------------------+
| Cluster  | Namespace                                        | Name                                                            | Expiration Date       |
+----------+--------------------------------------------------+-----------------------------------------------------------------+-----------------------+
| Cluster1 | openshift-apiserver-operator                     | openshift-apiserver-operator-serving-cert                       | Dec 18 15:20:22 2025  |
| Cluster1 | openshift-apiserver                              | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-apiserver                              | serving-cert                                                    | Dec 18 15:20:25 2025  |
| Cluster1 | openshift-authentication-operator                | serving-cert                                                    | Dec 18 15:20:24 2025  |
| Cluster1 | openshift-authentication                         | v4-0-config-system-serving-cert                                 | Dec 18 15:20:32 2025  |
| Cluster1 | openshift-cloud-credential-operator              | cloud-credential-operator-serving-cert                          | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-cloud-credential-operator              | pod-identity-webhook                                            | Dec 18 15:34:07 2025  |
| Cluster1 | openshift-cluster-csi-drivers                    | aws-ebs-csi-driver-controller-metrics-serving-cert              | Dec 18 15:20:32 2025  |
| Cluster1 | openshift-cluster-machine-approver               | machine-approver-tls                                            | Dec 18 15:20:21 2025  |
| Cluster1 | openshift-cluster-node-tuning-operator           | node-tuning-operator-tls                                        | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-cluster-node-tuning-operator           | performance-addon-operator-webhook-cert                         | Dec 18 15:20:19 2025  |
| Cluster1 | openshift-cluster-samples-operator               | samples-operator-tls                                            | Dec 18 15:23:04 2025  |
| Cluster1 | openshift-cluster-storage-operator               | cluster-storage-operator-serving-cert                           | Dec 18 15:20:23 2025  |
| Cluster1 | openshift-cluster-storage-operator               | csi-snapshot-webhook-secret                                     | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-cluster-storage-operator               | serving-cert                                                    | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-cluster-version                        | cluster-version-operator-serving-cert                           | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-config-managed                         | kube-controller-manager-client-cert-key                         | Jan 18 15:20:17 2024  |
| Cluster1 | openshift-config-managed                         | kube-scheduler-client-cert-key                                  | Jan 18 15:20:32 2024  |
| Cluster1 | openshift-config-operator                        | config-operator-serving-cert                                    | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-config                                 | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-config                                 | etcd-metric-client                                              | Dec 16 15:16:22 2033  |
| Cluster1 | openshift-config                                 | etcd-metric-signer                                              | Dec 16 15:16:22 2033  |
| Cluster1 | openshift-config                                 | etcd-signer                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-console-operator                       | serving-cert                                                    | Dec 18 15:33:41 2025  |
| Cluster1 | openshift-console-operator                       | webhook-serving-cert                                            | Dec 18 15:33:41 2025  |
| Cluster1 | openshift-console                                | console-serving-cert                                            | Dec 18 15:36:24 2025  |
| Cluster1 | openshift-controller-manager-operator            | openshift-controller-manager-operator-serving-cert              | Dec 18 15:20:27 2025  |
| Cluster1 | openshift-controller-manager                     | serving-cert                                                    | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-dns-operator                           | metrics-tls                                                     | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-dns                                    | dns-default-metrics-tls                                         | Dec 18 15:20:33 2025  |
| Cluster1 | openshift-etcd-operator                          | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-etcd-operator                          | etcd-operator-serving-cert                                      | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-etcd                                   | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-etcd                                   | etcd-peer-ip-10-0-207-102.us-east-2.compute.internal            | Dec 18 15:20:10 2026  |
| Cluster1 | openshift-etcd                                   | etcd-serving-ip-10-0-207-102.us-east-2.compute.internal         | Dec 18 15:20:10 2026  |
| Cluster1 | openshift-etcd                                   | etcd-serving-metrics-ip-10-0-207-102.us-east-2.compute.internal | Dec 18 15:20:10 2026  |
| Cluster1 | openshift-etcd                                   | serving-cert                                                    | Dec 18 15:20:17 2025  |
| Cluster1 | openshift-image-registry                         | image-registry-operator-tls                                     | Dec 18 15:20:20 2025  |
| Cluster1 | openshift-image-registry                         | image-registry-tls                                              | Dec 18 15:27:39 2025  |
| Cluster1 | openshift-ingress-operator                       | metrics-tls                                                     | Dec 18 15:20:19 2025  |
| Cluster1 | openshift-ingress-operator                       | router-ca                                                       | Dec 18 15:21:24 2025  |
| Cluster1 | openshift-ingress                                | router-metrics-certs-default                                    | Dec 18 15:21:24 2025  |
| Cluster1 | openshift-insights                               | openshift-insights-serving-cert                                 | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-kube-apiserver-operator                | aggregator-client-signer                                        | Jan 19 10:22:14 2024  |
| Cluster1 | openshift-kube-apiserver-operator                | kube-apiserver-operator-serving-cert                            | Dec 18 15:20:28 2025  |
| Cluster1 | openshift-kube-apiserver-operator                | kube-apiserver-to-kubelet-signer                                | Dec 18 15:10:07 2024  |
| Cluster1 | openshift-kube-apiserver-operator                | kube-control-plane-signer                                       | Dec 18 15:10:07 2024  |
| Cluster1 | openshift-kube-apiserver-operator                | loadbalancer-serving-signer                                     | Dec 16 15:10:03 2033  |
| Cluster1 | openshift-kube-apiserver-operator                | localhost-recovery-serving-signer                               | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver-operator                | localhost-serving-signer                                        | Dec 16 15:10:02 2033  |
| Cluster1 | openshift-kube-apiserver-operator                | node-system-admin-client                                        | Apr 17 15:20:23 2024  |
| Cluster1 | openshift-kube-apiserver-operator                | node-system-admin-signer                                        | Dec 18 15:20:10 2024  |
| Cluster1 | openshift-kube-apiserver-operator                | service-network-serving-signer                                  | Dec 16 15:10:02 2033  |
| Cluster1 | openshift-kube-apiserver                         | aggregator-client                                               | Jan 19 10:22:26 2024  |
| Cluster1 | openshift-kube-apiserver                         | check-endpoints-client-cert-key                                 | Jan 18 15:20:33 2024  |
| Cluster1 | openshift-kube-apiserver                         | control-plane-node-admin-client-cert-key                        | Jan 18 15:20:35 2024  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-10                                                  | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-2                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-3                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-4                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-5                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-6                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-7                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-8                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | etcd-client-9                                                   | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-kube-apiserver                         | external-loadbalancer-serving-certkey                           | Jan 18 15:20:19 2024  |
| Cluster1 | openshift-kube-apiserver                         | internal-loadbalancer-serving-certkey                           | Jan 18 15:20:36 2024  |
| Cluster1 | openshift-kube-apiserver                         | kubelet-client                                                  | Jan 18 15:20:18 2024  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey                              | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-10                           | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-2                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-3                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-4                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-5                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-6                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-7                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-8                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-recovery-serving-certkey-9                            | Dec 16 15:20:12 2033  |
| Cluster1 | openshift-kube-apiserver                         | localhost-serving-cert-certkey                                  | Jan 18 15:20:18 2024  |
| Cluster1 | openshift-kube-apiserver                         | service-network-serving-certkey                                 | Jan 18 15:20:19 2024  |
| Cluster1 | openshift-kube-controller-manager-operator       | csr-signer                                                      | Jan 19 10:24:17 2024  |
| Cluster1 | openshift-kube-controller-manager-operator       | csr-signer-signer                                               | Feb 18 10:22:43 2024  |
| Cluster1 | openshift-kube-controller-manager-operator       | kube-controller-manager-operator-serving-cert                   | Dec 18 15:20:24 2025  |
| Cluster1 | openshift-kube-controller-manager                | csr-signer                                                      | Jan 19 10:24:17 2024  |
| Cluster1 | openshift-kube-controller-manager                | kube-controller-manager-client-cert-key                         | Jan 18 15:20:17 2024  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert                                                    | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-2                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-3                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-4                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-5                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-6                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-7                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-8                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-controller-manager                | serving-cert-9                                                  | Dec 18 15:20:29 2025  |
| Cluster1 | openshift-kube-scheduler-operator                | kube-scheduler-operator-serving-cert                            | Dec 18 15:20:18 2025  |
| Cluster1 | openshift-kube-scheduler                         | kube-scheduler-client-cert-key                                  | Jan 18 15:20:32 2024  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert                                                    | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert-4                                                  | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert-5                                                  | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert-6                                                  | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert-7                                                  | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-scheduler                         | serving-cert-8                                                  | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-kube-storage-version-migrator-operator | serving-cert                                                    | Dec 18 15:20:18 2025  |
| Cluster1 | openshift-machine-api                            | cluster-autoscaler-operator-cert                                | Dec 18 15:20:26 2025  |
| Cluster1 | openshift-machine-api                            | cluster-baremetal-operator-tls                                  | Dec 18 15:20:20 2025  |
| Cluster1 | openshift-machine-api                            | cluster-baremetal-webhook-server-cert                           | Dec 18 15:20:25 2025  |
| Cluster1 | openshift-machine-api                            | control-plane-machine-set-operator-tls                          | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-machine-api                            | machine-api-controllers-tls                                     | Dec 18 15:20:17 2025  |
| Cluster1 | openshift-machine-api                            | machine-api-operator-tls                                        | Dec 18 15:20:23 2025  |
| Cluster1 | openshift-machine-api                            | machine-api-operator-webhook-cert                               | Dec 18 15:20:30 2025  |
| Cluster1 | openshift-machine-config-operator                | mcc-proxy-tls                                                   | Dec 18 15:20:22 2025  |
| Cluster1 | openshift-machine-config-operator                | proxy-tls                                                       | Dec 18 15:20:27 2025  |
| Cluster1 | openshift-marketplace                            | marketplace-operator-metrics                                    | Dec 18 15:20:23 2025  |
| Cluster1 | openshift-monitoring                             | alertmanager-main-tls                                           | Dec 18 15:33:01 2025  |
| Cluster1 | openshift-monitoring                             | cluster-monitoring-operator-tls                                 | Dec 18 15:20:16 2025  |
| Cluster1 | openshift-monitoring                             | kube-state-metrics-tls                                          | Dec 18 15:27:58 2025  |
| Cluster1 | openshift-monitoring                             | node-exporter-tls                                               | Dec 18 15:27:58 2025  |
| Cluster1 | openshift-monitoring                             | openshift-state-metrics-tls                                     | Dec 18 15:27:58 2025  |
| Cluster1 | openshift-monitoring                             | prometheus-adapter-tls                                          | Dec 18 15:27:58 2025  |
| Cluster1 | openshift-monitoring                             | prometheus-k8s-thanos-sidecar-tls                               | Dec 18 15:33:02 2025  |
| Cluster1 | openshift-monitoring                             | prometheus-k8s-tls                                              | Dec 18 15:33:03 2025  |
| Cluster1 | openshift-monitoring                             | prometheus-operator-admission-webhook-tls                       | Dec 18 15:20:32 2025  |
| Cluster1 | openshift-monitoring                             | prometheus-operator-tls                                         | Dec 18 15:27:49 2025  |
| Cluster1 | openshift-monitoring                             | telemeter-client-tls                                            | Dec 18 15:27:58 2025  |
| Cluster1 | openshift-monitoring                             | thanos-querier-tls                                              | Dec 18 15:27:56 2025  |
| Cluster1 | openshift-multus                                 | metrics-daemon-secret                                           | Dec 18 15:20:25 2025  |
| Cluster1 | openshift-multus                                 | multus-admission-controller-secret                              | Dec 18 15:20:21 2025  |
| Cluster1 | openshift-network-operator                       | metrics-tls                                                     | Dec 18 15:20:27 2025  |
| Cluster1 | openshift-oauth-apiserver                        | etcd-client                                                     | Dec 16 15:16:21 2033  |
| Cluster1 | openshift-oauth-apiserver                        | serving-cert                                                    | Dec 18 15:20:31 2025  |
| Cluster1 | openshift-operator-lifecycle-manager             | catalog-operator-serving-cert                                   | Dec 18 15:20:28 2025  |
| Cluster1 | openshift-operator-lifecycle-manager             | olm-operator-serving-cert                                       | Dec 18 15:20:17 2025  |
| Cluster1 | openshift-operator-lifecycle-manager             | packageserver-service-cert                                      | Dec 18 15:20:53 2025  |
| Cluster1 | openshift-operator-lifecycle-manager             | pprof-cert                                                      | Dec 26 12:15:02 2023  |
| Cluster1 | openshift-route-controller-manager               | serving-cert                                                    | Dec 18 15:20:21 2025  |
| Cluster1 | openshift-sdn                                    | sdn-controller-metrics-certs                                    | Dec 18 15:20:17 2025  |
| Cluster1 | openshift-sdn                                    | sdn-metrics-certs                                               | Dec 18 15:20:26 2025  |
| Cluster1 | openshift-service-ca-operator                    | serving-cert                                                    | Dec 18 15:20:19 2025  |
| Cluster1 | openshift-service-ca                             | signing-key                                                     | Feb 16 15:20:11 2026  |
+----------+--------------------------------------------------+-----------------------------------------------------------------+-----------------------+
```

```
Certificates About to Expire:
Cluster1 | openshift-config-managed | kube-controller-manager-client-cert-key | Expires on Jan 18 15:20:17 2024 
Cluster1 | openshift-config-managed | kube-scheduler-client-cert-key | Expires on Jan 18 15:20:32 2024 
Cluster1 | openshift-kube-apiserver-operator | aggregator-client-signer | Expires on Jan 19 10:22:14 2024 
Cluster1 | openshift-kube-apiserver | aggregator-client | Expires on Jan 19 10:22:26 2024 
Cluster1 | openshift-kube-apiserver | check-endpoints-client-cert-key | Expires on Jan 18 15:20:33 2024 
Cluster1 | openshift-kube-apiserver | control-plane-node-admin-client-cert-key | Expires on Jan 18 15:20:35 2024 
Cluster1 | openshift-kube-apiserver | external-loadbalancer-serving-certkey | Expires on Jan 18 15:20:19 2024 
Cluster1 | openshift-kube-apiserver | internal-loadbalancer-serving-certkey | Expires on Jan 18 15:20:36 2024 
Cluster1 | openshift-kube-apiserver | kubelet-client | Expires on Jan 18 15:20:18 2024 
Cluster1 | openshift-kube-apiserver | localhost-serving-cert-certkey | Expires on Jan 18 15:20:18 2024 
Cluster1 | openshift-kube-apiserver | service-network-serving-certkey | Expires on Jan 18 15:20:19 2024 
Cluster1 | openshift-kube-controller-manager-operator | csr-signer | Expires on Jan 19 10:24:17 2024 
Cluster1 | openshift-kube-controller-manager | csr-signer | Expires on Jan 19 10:24:17 2024 
Cluster1 | openshift-kube-controller-manager | kube-controller-manager-client-cert-key | Expires on Jan 18 15:20:17 2024 
Cluster1 | openshift-kube-scheduler | kube-scheduler-client-cert-key | Expires on Jan 18 15:20:32 2024 
Cluster1 | openshift-operator-lifecycle-manager | pprof-cert | Expires on Dec 26 12:15:02 2023 
```
